const express = require('express')
const app = express()

app.use('/cv', express.static('dist'))

app.listen(3000, () => console.log('Listen at 3000'))