import "../scss/style.scss"

const modal = document.getElementById('modal')

const modalContent = document.createElement('DIV')
modalContent.classList.add('modalContent')

const modalContentText = document.createElement('DIV')
modalContentText.classList.add('modalContent__text')
modalContentText.textContent = "Click the button to download my resume!"

const modalContentButton = document.createElement('A')
modalContentButton.classList.add('modalContent__button')
modalContentButton.setAttribute('href', './resume.pdf')
modalContentButton.setAttribute('download', '')
modalContentButton.textContent = "Download"

modalContent.appendChild(modalContentText)
modalContent.appendChild(modalContentButton)
modal.appendChild(modalContent)