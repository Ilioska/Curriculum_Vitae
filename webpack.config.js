const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require("clean-webpack-plugin")
const devMode = process.env.NODE_ENV !== 'production'
console.log(process.env.NODE_ENV)

module.exports = {
    mode: 'development',
    entry: process.env.NODE_ENV === 'pdf' ? './src/js/index-pdf.js' : './src/js/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js' 
    },
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: [ 
                    devMode ? MiniCssExtractPlugin.loader : 'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff2?|eot|ttf|otf|svg)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        new CleanWebpackPlugin('./dist')
    ],
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist'
    }
}
